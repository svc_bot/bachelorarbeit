public class ProgSmallIf {

    public static void main(String[] args) {
        int a = 2;
        if (a > 0) {
            a = a + 1;
            System.out.println("a > 0");
        } else {
            System.out.println("a < 0");
        }
    }
}
