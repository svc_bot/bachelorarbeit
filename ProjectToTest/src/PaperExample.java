import java.util.Random;

public class PaperExample {
    public static void main(String[] args) {
        int a,b;
        Random rnd = new Random();

        while (true) {
            a = rnd.nextInt(3);
            b = rnd.nextInt(3);
            if ((a + 1) % 3 == b) {
                System.out.println("a wins");
                break;
            } else if ((b + 1) % 3 == a) {
                System.out.println("b wins");
                break;
            }
        }
    }
}
