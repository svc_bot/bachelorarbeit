
public class Prog3While {

	public static void main(String[] args) {
		int x = 1, a = 0;
		while (x + a < 32) {
			if (x % 2 == 0) a++;
			x += a;
		}

	}

}
