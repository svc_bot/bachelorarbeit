#/bin/bash

#written by Eric Bodden, McGill University,
#use at own risk

### USER DEFINED VARIABLES ###

#java runtime to analyze with Soot
JRE=/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/

#location of Soot
SOOT=/home/alex/workspace/bachelorarbeit/SparseAnalysis/bin/

# location of DaCapo
DACAPO=/home/alex/Dokumente/Bachelorarbeit/dacapo-9.12-bach.jar

#custom command line options for Soot
CUSTOM_OPTIONS=

#temporary directory in which to extract the benchmarks as input for Soot
TEMP=/tmp/dacapo

### END OF USER DEFINED VARIABLES ###


for BM in \
        `java -jar $DACAPO -l`
do \

#prepare benchmark
echo preparing ${BM}...
rm -fr ${TEMP}
mkdir -p ${TEMP}
cp ${BM}.jar ${TEMP}
OLDDIR=`pwd`
cd ${TEMP}
jar xf ${BM}.jar
cd ${OLDDIR}

#process benchmark
echo processing ${BM}...

java -Xmx512m -cp ${SOOT} \
common.Main \
-process-dir ${TEMP} \
-soot-class-path ${JRE}/lib/rt.jar:${JRE}/lib/jce.jar:${JRE}/lib/jsse.jar:${BM}-deps.jar:${TEMP} \
-main-class dacapo.${BM}.Main \
${CUSTOM_OPTIONS}

echo finished with ${BM}.

done

