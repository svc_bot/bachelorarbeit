#!/bin/bash

# set variables
# SOOT=/home/alex/Dokumente/Bachelorarbeit/soot-2.5.0.jar # path to soor.jar directory
ANALYIS=/home/alex/workspace/bachelorarbeit/SparseAnalysis/bin/
DACAPO=/home/alex/Dokumente/Bachelorarbeit/dacapo-9.12-bach.jar # path to dacapo.jar
JVM=/usr/lib/jvm/jdk1.8.0_91/bin/java # path to java
TEMP=/tmp/dacapo

for SIZE in small default large huge
do
	for BM in `${JVM} -jar ${DACAPO} -l`
	do		
		COMMAND="${JVM} -cp ${ANALYIS} common.Main\ 
		-javaagent:pia.jar=${INDIR} Harness ${DACAPOOPTS2} $BM -s $SIZE"

		echo running $BM $SIZE  with classes from ${INDIR}...
		echo $COMMAND > ${INDIR}/run.log 2>&1
		eval $COMMAND >> ${INDIR}/run.log 2>&1
	done
done
