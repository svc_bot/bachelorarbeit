#!/bin/bash

# set variables
SOOT=/home/alex/Dokumente/Bachelorarbeit/
ANALYSIS=/home/alex/workspace/bachelorarbeit/simple.SparseAnalysisOld/
JRE=/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/lib/rt.jar
TEST=/home/alex/workspace/bachelorarbeit/ProjectToTest/

# compile files before using them
echo "compiling classes"
javac -sourcepath ${ANALYSIS}/src/*.java -d ${ANALYSIS}/bin/

# run soot

java -cp ${SOOT}/soot-2.5.0.jar:${ANALYSIS}/bin/ Test4 -cp $JRE:${TEST}/bin Prog4Loop
