package simple;

import improved.PathDesignator;

import java.util.BitSet;
import java.util.HashSet;

public class VarPredicate {
    private BitSet predicate = new BitSet();
    private HashSet<String> phiFuncExtras = new HashSet<>();
    private HashSet<String> assertionExtras = new HashSet<>();
    private HashSet<PathDesignator> designatorSet = new HashSet<>();

    public VarPredicate(BitSet predicate) {
        this.predicate = predicate;
    }

    public VarPredicate(BitSet predicate, HashSet<String> phiFuncExtras) {
        this.predicate = predicate;
        this.phiFuncExtras = phiFuncExtras;
    }

    public VarPredicate(VarPredicate varPredicate) {
        this.predicate = (BitSet) varPredicate.predicate.clone();
        this.phiFuncExtras = (HashSet<String>) varPredicate.phiFuncExtras.clone();
        this.assertionExtras = (HashSet<String>) varPredicate.getAssertionExtras().clone();
    }

    /**
     * @return deep copy of the VarPredicate object
     */
    public VarPredicate copy() {
        return new VarPredicate(this);
    }

    public BitSet getPredicate() {
        return predicate;
    }

    public HashSet<String> getPhiFuncExtras() {
        return phiFuncExtras;
    }

    public HashSet<String> getAssertionExtras() {
        return assertionExtras;
    }

    public HashSet<PathDesignator> getDesignatorSet() { return designatorSet; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VarPredicate)) return false;

        VarPredicate that = (VarPredicate) o;

        if (!predicate.equals(that.predicate)) return false;
        if (!phiFuncExtras.equals(that.phiFuncExtras)) return false;
        if (!assertionExtras.equals(that.assertionExtras)) return false;
        return designatorSet.equals(that.designatorSet);

    }

    @Override
    public int hashCode() {
        int result = predicate.hashCode();
        result = 31 * result + phiFuncExtras.hashCode();
        result = 31 * result + assertionExtras.hashCode();
        result = 31 * result + designatorSet.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean needComma = false;
        needComma = false;
        sb.append("{");
        for (int i = 0; i < getPredicate().length(); i++) {
            if (getPredicate().get(i)) {
                if (needComma) {sb.append(", "); }
                sb.append(i);
                needComma = true;
            }
        }
        if (getPhiFuncExtras().size() > 0) {
            for (String str : getPhiFuncExtras()){
                if (needComma) {sb.append(", "); }
                sb.append(str);
                needComma = true;
            }
        }
        sb.append("}");

        return sb.toString();
    }

}
