package simple;

import common.*;
import org.junit.Assert;
import soot.*;
import soot.jimple.Constant;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JIfStmt;
import soot.shimple.PhiExpr;
import soot.shimple.toolkits.scalar.ShimpleLocalDefs;
import soot.shimple.toolkits.scalar.ShimpleLocalUses;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.UnitValueBoxPair;

import java.util.*;

import static common.InstructionType.*;
import static common.InstructionType.none;

public class SparseAnalysis implements Analysis {
	protected DirectedGraph<Block> graph;
    protected ShimpleLocalDefs localDefs; // returns definition nodes of instruction
    protected ShimpleLocalUses localUses; // returns nodes that use instruction
    protected List<Unit> nodesToAnalyse; // nodes on which we perform our analysis, other nodes can be ignored for now
    protected Map<Unit, Integer> nodeToIndex; // contains indexes of every node
    protected Map<Integer, Unit> indexToNode; // contains reference to every node by its index
    protected HashMap<Integer, BitSet> killSet;
    protected HashMap<Integer, HashSet<String>> killPhiExtras; // map block index -> set of phi-func extras, because they are equal for the whole block
    protected HashMap<Integer, Assertion> blockToAssertion;
    protected List<Unit> nodesWithSubstitutions;
    protected int[] nodeToBlockIndex;
    public  Map<Integer, HashSet<VarPredicate>> pathPredicates;
    public long analysisDuration = 0;

	public SparseAnalysis(DirectedGraph<Block> graph, ShimpleLocalDefs localDefs, ShimpleLocalUses localUses) {

		this.graph = graph;
		this.localDefs = localDefs;
        this.localUses = localUses;
        this.nodesToAnalyse = new ArrayList<>(); // we want to operate on nodes of local type
        indexToNode = new HashMap<Integer, Unit>();
        nodeToIndex = new HashMap<Unit, Integer>();
        killSet = new HashMap<Integer, BitSet>();
        killPhiExtras = new HashMap<Integer, HashSet<String>>();
        blockToAssertion = new HashMap<>();
        nodesWithSubstitutions = new ArrayList<>();
        pathPredicates = new HashMap<Integer, HashSet<VarPredicate> >();
        nodeToBlockIndex = new int[graph.iterator().next().getBody().getUnits().size()]; // beware, .getBody() returns the body of whole graph, not of a single block

        setUpIndexesAndFields();
        genereteKillsets();
        preprocess();
        long startTime = System.currentTimeMillis();
        doAnalysis();
        analysisDuration = System.currentTimeMillis() - startTime;

    }

    /**
     * what is to be done:
     * identify all if-statements - ok
     * goto block in which target is contained, then add new node containing asserted value - ok
     * x_new = assert(x_old op const);
     * then find if the node from the condition is used, then replace in that node x_old with x_new
     * before: condition= x_2 % 2 == 0; x_3 = x_2 op whatever
     * after:  condition= x_2 % 2 == 0; x_new = assert(x_2 % 2 == 0); x_3 = x_new op whatever
     */
    private  void preprocess() {
        ArrayList<Unit> listOfIfNodes = new ArrayList<>();
        for (Unit node : graph.iterator().next().getBody().getUnits()) {
            if (node instanceof JIfStmt){
                listOfIfNodes.add(node);
                Value conditionValue = ((JIfStmt) node).getCondition();
                Unit target = ((JIfStmt) node).getTargetBox().getUnit();
                int targetInBLock = nodeToBlockIndex[nodeToIndex.get(target)];
                Block targetBlock = null;
                // find block
                Iterator iter = graph.iterator();
                while (iter.hasNext()) {
                    Block block = (Block) iter.next();
                    if (block.getIndexInMethod() == targetInBLock) {
                        if (canSkipBlock(targetInBLock)) {
                            // the target block consists only of nopes and gotos, so the block we need is the next successor
                            targetBlock = block.getSuccs().get(0);
                        } else targetBlock = block;
                    }
                }

                // add (a_1, ..., a_n) = assert(f(a_1, ... , a_n))  to map
                //System.out.println("need to add assert(" + conditionValue + ") in block " + targetInBLock);
                List<ValueBox> conditionUses = conditionValue.getUseBoxes();
                List<Value> conditionValues = new ArrayList<>();
                for (ValueBox use : conditionUses) {
                    conditionValues.add(use.getValue());
                }
                blockToAssertion.put(targetBlock.getIndexInMethod(), new Assertion(conditionValue, targetBlock.getIndexInMethod()));

                // mark nodes where substitution is needed
                Iterator blockIterator = targetBlock.iterator();
                while (blockIterator.hasNext()) {
                    Unit nextNode = (Unit) blockIterator.next();
                    if (getType(nextNode) != none) {
                        Value nextNodeRightOp = ((JAssignStmt) nextNode).getRightOp();
                        if (conditionValues.contains(nextNodeRightOp)) {
                            nodesWithSubstitutions.add(nextNode);
                        } else {
                            for (Object valueBox : nextNodeRightOp.getUseBoxes()) {
                                if (conditionValues.contains(((ValueBox) valueBox).getValue())) {
                                    nodesWithSubstitutions.add(nextNode);
                                }
                            }
                        }
                    }
                }
//                System.out.println("nodes with substitutions: " + nodesWithSubstitutions);
            }
        }

    }

    private boolean canSkipBlock(int blockIndex) {
        Iterator iter = graph.iterator();
        while (iter.hasNext()) {
            Block block = (Block) iter.next();
            if (block.getIndexInMethod() == blockIndex) {
                Iterator blockIterator = block.iterator();
                while (blockIterator.hasNext()) {
                    Unit node = (Unit) blockIterator.next();
                    if (getType(node) != none) return false;
                }
                if (!block.getSuccs().isEmpty()) return true;
            }
        }
        return false;
    }

    protected void genereteKillsets() {
        // generate kill sets for nodes
        for (Unit node : nodesToAnalyse){
            BitSet nodesKills = new BitSet(nodesToAnalyse.size());
            for (Unit otherNode : nodesToAnalyse) {
                if (!node.equals(otherNode)) {
                    //System.out.println("current node: " + otherNode);
                    Value operandsValue = ((JAssignStmt)otherNode).getRightOp();
                    List<Value> operandsValueList = new ArrayList<>();
                    //System.out.println("list of operands boxes " + operandsValue);
                    List<Unit> operands = new ArrayList<>();
                    if (operandsValue instanceof Local) {
                        operandsValueList.add(operandsValue);
                    } else {
                        for (Object valueBox : operandsValue.getUseBoxes()) {
                            operandsValueList.add(((ValueBox) valueBox).getValue());
                        }
                    }
                    for (Value value : operandsValueList) {
                        if (value instanceof Constant) continue;
                        operands.add(localDefs.getDefsOf((Local) value).get(0));
                    }
                    //System.out.println(" operands " + operands);
                    if (!operands.contains(node)) {
                        nodesKills.set(nodeToIndex.get(otherNode));
                    }
                }
            }
            killSet.put(nodeToIndex.get(node), nodesKills);
        }
        // check the killset
        System.out.println("killset: ");
        System.out.println(killSet);

        // generate killsets for phi function extras
        // need to kill those phi extras, where x is used or defined
        for (Unit phinode : nodesToAnalyse) {
            if (getType(phinode) == phiFunction) {

                killPhiExtras.put(nodeToIndex.get(phinode), new HashSet<String>());

                List<Unit> operands = new ArrayList<>();
                List<ValueBox> operandValues = ((JAssignStmt)phinode).getRightOp().getUseBoxes();

                for (ValueBox valueBox : operandValues) {
                    operands.add(localDefs.getDefsOf((Local) valueBox.getValue()).get(0));
                }

                for (Unit operand : operands) {
                    killPhiExtras.get(nodeToIndex.get(phinode)).add(phinode.getDefBoxes().get(0).getValue() +
                            " = " + operand.getDefBoxes().get(0).getValue());
                }

            }
        }

        // check the killset of phi-function extras
        System.out.println("kill set of phi-function extras");
        System.out.println(killPhiExtras);
        System.out.println();
    }

    /**
     * sets all indexes in indexToNode, nodeToIndex and nodeToBlockIndex sets,
     * and fills nodesToAnalyse list and initializes pathPredicates Map
     */
    protected void setUpIndexesAndFields(){
        int nodeIndex   = 0;
        int blockIndex  = 0;

        for (Block block : graph){
            Iterator<Unit> blockIterator = block.iterator();
            while (blockIterator.hasNext()) {
                Unit node = blockIterator.next();
                if (getType(node) != none) {
                    nodesToAnalyse.add(node);
                }
                indexToNode.put(nodeIndex, node);
                nodeToIndex.put(node, nodeIndex);
                nodeToBlockIndex[nodeIndex] = blockIndex;
                ++nodeIndex;
            }
            ++blockIndex;
        }

        for (Unit node : nodesToAnalyse) {
            killPhiExtras.put(nodeToIndex.get(node), new HashSet<String>());
        }

        for (int i : indexToNode.keySet()) {
            pathPredicates.put(i, new HashSet<VarPredicate>());
        }
    }

    /**
     * Version of Analysis without path designators
     */
    public void doAnalysis() {
        G.v().out.println("Doing analysis without path designators: ");
        G.v().out.println();
        // output instructions to console + sanity check
        // block view
        System.out.println("Analysing " + graph.iterator().next().getBody().getMethod().getName());
        System.out.println("Block view of the program: ");
        for (Block block : graph) {
            System.out.println(block);
        }
        // writes instructions to console
        System.out.println("Nodes to analyse: ");
        for (Unit node : nodesToAnalyse){
            System.out.println(nodeToIndex.get(node) + ": " + node + " in block " + nodeToBlockIndex[nodeToIndex.get(node)]);
        }

        List<Unit> worklist = new ArrayList<>();
        worklist.addAll(nodesToAnalyse);

        while (!worklist.isEmpty()){
            Unit nextNode = worklist.remove(0); // pop first element from the list and use it later
            HashSet<VarPredicate> newPred = computePredOf(nextNode);
            HashSet<VarPredicate> oldPred = pathPredicates.get(nodeToIndex.get(nextNode));

            if(!newPred.equals(oldPred)){
                pathPredicates.put(nodeToIndex.get(nextNode), newPred);
                List<UnitValueBoxPair> updateQueue = localUses.getUsesOf(nextNode);
                if(updateQueue == null) continue; // nothing to do, skip to next instruction
                for (UnitValueBoxPair uvbox : updateQueue) {
                    Unit queuedUnit = uvbox.getUnit();
                    if(!worklist.contains(queuedUnit)) {
                        worklist.add(queuedUnit);
                    }
                }
            }
        }

        G.v().out.println("Done.");
        G.v().out.println("Check the results!");
        G.v().out.println("computed path predicates: ");

        for (Unit node : nodesToAnalyse) {
            if (nodesWithSubstitutions.contains(node)) {
                Assertion assertion = blockToAssertion.get(nodeToBlockIndex[nodeToIndex.get(node)]);
                String nodesView = node.toString();
                ArrayList<Value> rightSideValues = getNodesValues(node);
                for (Value value : rightSideValues) {
                    if (assertion.getAssertedValues().contains(value)){
                        nodesView = nodesView.replaceAll(" " + value.toString(), " " + value + "_asserted");
                    }
                }
                System.out.println(nodeToIndex.get(node) + ": " + nodesView + " -> " + pathPredicates.get(nodeToIndex.get(node)));
            } else {
                System.out.println(nodeToIndex.get(node) + ": " + node + " -> " + pathPredicates.get(nodeToIndex.get(node)));
            }
        }
        System.out.println("----------");

	}

    @Override
    public long getDuration() {
        return analysisDuration;
    }

    @Override
    public int getNodesCount() {
        return nodesToAnalyse.size();
    }

    @Override
    public int getPredicatesCount() {
        int count = 0;

        for (Integer index : pathPredicates.keySet()) {
            count += pathPredicates.get(index).size();
        }

        return count;
    }

    private ArrayList<Value> getNodesValues(Unit node) {
        Value operandsValue = ((JAssignStmt)node).getRightOp();
        ArrayList<Value> operandsValueList = new ArrayList<>();
        if (operandsValue instanceof Local) {
            operandsValueList.add(operandsValue);
        } else {
            for (Object valueBox : operandsValue.getUseBoxes()) {
                operandsValueList.add(((ValueBox) valueBox).getValue());
            }
        }
        return operandsValueList;
    }

    private HashSet<VarPredicate> computePredOf(Unit node) {
        HashSet<VarPredicate> computedPred = new HashSet<>();
        HashSet<VarPredicate> ypreds       = new HashSet<>();
        HashSet<VarPredicate> zpreds 	   = new HashSet<>();

        switch (getType(node)) {
            case constAssign:
                BitSet bitSet = new BitSet(nodesToAnalyse.size());
                bitSet.set(nodeToIndex.get(node));
                computedPred.add(new VarPredicate(bitSet));
                break;
            case varAssign:
                // x = y
                // in this case we substitute x = y with
                // (...,y,..) = assert(... y ...)
                // x = y_asserted

//                for (VarPredicate varPredicate : assertionPred) {
//                    VarPredicate newPredicate = varPredicate.copy();
//                    newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
//                    newPredicate.getPredicate().set(nodeToIndex.get(node));
//                    newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
//                    computedPred.add(newPredicate);
//                }
                Value rightSideValue = node.getUseBoxes().get(0).getValue(); // getUseBoxes returns a list containing only one element, which itself is another list
                List<Unit> listDefs = localDefs.getDefsOf((Local) rightSideValue); // in this case should always have max 1 element

                if (nodesWithSubstitutions.contains(node)) {
                    Assertion assertion = blockToAssertion.get(nodeToBlockIndex[nodeToIndex.get(node)]);
                    ypreds = computePredOf(assertion);
                } else {
                    ypreds = pathPredicates.get(nodeToIndex.get(listDefs.get(0))); // get predicates of y
                }

                if (!listDefs.isEmpty() && ypreds == null) {
                    // this happens on virtual and special invokes
                    BitSet bitSetSpecialCase = new BitSet(nodesToAnalyse.size());
                    bitSetSpecialCase.set(nodeToIndex.get(node));
                    computedPred.add(new VarPredicate(bitSetSpecialCase));
                } else {
                    for(VarPredicate varPredicate : ypreds){
                        VarPredicate newPredicate = varPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                }

                break;
            case complexAssign:
                // x = y + z, y or z (exclusively) can be constant
                List<ValueBox> rightOps = node.getUseBoxes(); // list contains value boxes of y and z
                boolean substitutionNeeded = false;
                if (nodesWithSubstitutions.contains(node)) {
                    substitutionNeeded = true;
                }
                // In Shimple there can be max 2 values
                if (!(rightOps.get(0).getValue() instanceof Constant) && (rightOps.get(0).getValue() instanceof Local)){
                    if (substitutionNeeded) {
                        Assertion assertion = blockToAssertion.get(nodeToBlockIndex[nodeToIndex.get(node)]);
                        ypreds = computePredOf(assertion);
                    } else {
                        Unit yOperand = localDefs.getDefsOf((Local) rightOps.get(0).getValue()).get(0);
                        ypreds = pathPredicates.get(nodeToIndex.get(yOperand));
                    }
                }
                if (!(rightOps.get(1).getValue() instanceof Constant) && (rightOps.get(1).getValue() instanceof Local)){
                    if (substitutionNeeded) {
                        Assertion assertion = blockToAssertion.get(nodeToBlockIndex[nodeToIndex.get(node)]);
                        zpreds = computePredOf(assertion);
                    } else {
                        Unit zOperand = localDefs.getDefsOf((Local) rightOps.get(1).getValue()).get(0);
                        zpreds = pathPredicates.get(nodeToIndex.get(zOperand));
                    }
                }
                if(ypreds == null) ypreds = new HashSet<>();
                if(zpreds == null) zpreds = new HashSet<>();
                // union over preds of y and z
                if (ypreds.isEmpty()){
                    for (VarPredicate zVarPredicate : zpreds){
                        VarPredicate newPredicate = zVarPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                } else if(zpreds.isEmpty()){
                    for (VarPredicate yVarPredicate : ypreds){
                        VarPredicate newPredicate = yVarPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                } else {
                    for (VarPredicate yVarPredicate : ypreds) {
                        for (VarPredicate zVarPredicate : zpreds){
                            VarPredicate newPredicate = yVarPredicate.copy();
                            // compute new predicate BitSet
                            newPredicate.getPredicate().or(zVarPredicate.getPredicate());
                            newPredicate.getPhiFuncExtras().addAll(zVarPredicate.getPhiFuncExtras());
                            newPredicate.getAssertionExtras().addAll(zVarPredicate.getAssertionExtras());
                            newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                            newPredicate.getPredicate().set(nodeToIndex.get(node));
                            //compute new phiFuncExtras
                            newPredicate.getPhiFuncExtras().addAll(zVarPredicate.getPhiFuncExtras());
                            newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                            computedPred.add(newPredicate);

                        }
                    }
                }
                break;
            case phiFunction:
                // get everything needed for computation
                //--------------------------------------
                List<ValueBox> operandsUseBoxes = ((JAssignStmt) node).getRightOp().getUseBoxes(); // get list of values used inside of the definition of the node
                ArrayList<Unit> uses = new ArrayList<>();
                BitSet killSetIntersection = getPhiNodeKillsetsIntersection(node);
                for (ValueBox useBox : operandsUseBoxes){
                    Unit operandsNode = localDefs.getDefsOf((Local) useBox.getValue()).get(0);
                    uses.add(operandsNode);
                }

                for (Unit operand : uses) {
                    HashSet<VarPredicate> operandsPredicates = pathPredicates.get(nodeToIndex.get(operand));
                    // this happens when operand not belongs into list of nodes to analyse
                    if (operandsPredicates != null) {
                        for (VarPredicate varPredicate : operandsPredicates) {
                            VarPredicate newPredicate = varPredicate.copy();
                            newPredicate.getPredicate().and(killSetIntersection);
                            newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                            newPredicate.getPhiFuncExtras().add(node.getDefBoxes().get(0).getValue() + " = " + operand.getDefBoxes().get(0).getValue());
                            computedPred.add(newPredicate);
                        }
                    }

                }
                break;
            case none:
                break;
        }

        return computedPred;
    }

    private HashSet<VarPredicate> computePredOf(Assertion assertion) {
        HashSet<VarPredicate> result = new HashSet<>();
        ArrayList<Value> conditionValues = assertion.getAssertedValues();
        ArrayList<Unit> conditionNodes = new ArrayList<>();
        for (Value conditionOperand : conditionValues) {
            if (conditionOperand instanceof Local) {
                conditionNodes.add(localDefs.getDefsOf((Local) conditionOperand).get(0));
            } else {
                for (Object valueBox : conditionOperand.getUseBoxes()) {
                    conditionNodes.add(localDefs.getDefsOf((Local) ((ValueBox) valueBox).getValue()).get(0));
                }
            }
        }

        // generate kill set
        BitSet killset = new BitSet(nodesToAnalyse.size());
        killset.flip(0, nodesToAnalyse.size()); // since we know, that all nodes that use this new node are in one block we can reduce our search field
        int blockindex = assertion.getBlockIndex();
        Block targetBlock = null;
        Iterator iter = graph.iterator();
        while (iter.hasNext()) {
            Block block = (Block) iter.next();
            if (block.getIndexInMethod() == blockindex) {
                targetBlock = block;
                break;
            }
        }
        Iterator blockiter = targetBlock.iterator();
        while (blockiter.hasNext()) {
            Unit nextNode = (Unit) blockiter.next();
            if (getType(nextNode) != none) {
                Value operandsValue = ((JAssignStmt)nextNode).getRightOp();
                List<Value> operandsValueList = new ArrayList<>();
                if (operandsValue instanceof Local) {
                    operandsValueList.add(operandsValue);
                } else {
                    for (Object valueBox : operandsValue.getUseBoxes()) {
                        operandsValueList.add(((ValueBox) valueBox).getValue());
                    }
                }
                if (operandsValueList.contains(conditionValues.get(0))) {
                    killset.clear(nodeToIndex.get(nextNode));
                }
            }
        }

        // now construct new predicate for this assertion
        // get predicate of a from a op b
        if (conditionNodes.size() == 2) {
            // get predicates of a and b
            HashSet<VarPredicate> conditionPredicate = pathPredicates.get(nodeToIndex.get(conditionNodes.get(0)));
            HashSet<VarPredicate> conditionPredicate2 = pathPredicates.get(nodeToIndex.get(conditionNodes.get(1)));
            HashSet<VarPredicate> assertionPredicate = new HashSet<>();
            // construct pred(a) ∪ pred(b)
            for (VarPredicate varPredicate : conditionPredicate) {
                for (VarPredicate varPredicate2 : conditionPredicate2) {
                    VarPredicate newPredicate = varPredicate.copy();
                    //
                    newPredicate.getPredicate().or(varPredicate2.getPredicate());
                    newPredicate.getPhiFuncExtras().addAll(varPredicate2.getPhiFuncExtras());
                    newPredicate.getAssertionExtras().addAll(varPredicate2.getAssertionExtras());
                    //
                    assertionPredicate.add(newPredicate);
                }
            }
            //
            for (VarPredicate varPredicate : assertionPredicate) {
                varPredicate.getPredicate().and(killset);
                varPredicate.getAssertionExtras().add(assertion.getCondition().toString());
                for (Value value : assertion.getSubstitution().keySet()) {
                    varPredicate.getAssertionExtras().add(assertion.getSubstitution().get(value) + " = " + value);
                }
                result.add(varPredicate);
            }
        } else if (conditionNodes.size() == 1) {
            HashSet<VarPredicate> assertionPredicate = pathPredicates.get(nodeToIndex.get(conditionNodes.get(0)));
            // it can happen, that condition node is not in list of nodes to analyse
            if (assertionPredicate != null) {
                for (VarPredicate varPredicate : assertionPredicate) {
                    VarPredicate newPredicate = varPredicate.copy();
                    newPredicate.getPredicate().and(killset);
                    newPredicate.getAssertionExtras().add(assertion.getCondition().toString());
                    newPredicate.getAssertionExtras().add(assertion.getSubstitution().get(assertion.getAssertedValues().get(0)) + " = " + assertion.getAssertedValues().get(0));
                    result.add(newPredicate);
                }
            }

        }
        return result;
    }

    /**
     *
     * @param phinode is a unit of phi-function type
     * @return intersection of killsets of all phi-functions inside a block, where given phi-node is located
     */
    protected BitSet getPhiNodeKillsetsIntersection(Unit phinode){
        Assert.assertTrue(getType(phinode) == phiFunction); // sanity check
        int blockIndex = nodeToBlockIndex[nodeToIndex.get(phinode)];
        BitSet intersection = new BitSet(nodesToAnalyse.size());
        intersection.flip(0, nodesToAnalyse.size()); //set all to true
        Block blockToTraverse = null;

        // find the right block
        Iterator iter = graph.iterator();
        while (iter.hasNext()) {
            Block nextBlock = (Block) iter.next();
            if (nextBlock.getIndexInMethod() == blockIndex){
                blockToTraverse = nextBlock;
                break;
            }
        }
        // traverse the block and compute intersection of all phi-function killsets
        if (blockToTraverse != null) {
            Iterator<Unit> blockIterator = blockToTraverse.iterator();
            while (blockIterator.hasNext()) {
                Unit node = blockIterator.next();
                if (getType(node) == phiFunction) {
                    intersection.and(killSet.get(nodeToIndex.get(node)));
                }
            }
        }

        return intersection;
    }

    protected InstructionType getType(Unit node){
        if (node instanceof JAssignStmt) {
            if(((JAssignStmt) node).getRightOp() instanceof PhiExpr) return phiFunction;
            if(((JAssignStmt) node).getRightOp() instanceof Constant)return constAssign;
            else {
                if ( ((JAssignStmt) node).getRightOp() instanceof Local){
                    return varAssign;
                }
                else if (((JAssignStmt) node).getRightOp().getUseBoxes().size() >= 2) {
                    return complexAssign;
                }
            }
        }
        return none;
    }

    /**
     *
     * @param node
     * @return String representation of the node predicates
     */
    protected String getPredicateString(Unit node){
        String pred = "{";
        HashSet<VarPredicate> npreds = pathPredicates.get(nodeToIndex.get(node));
        boolean needComma = false;
        for (VarPredicate vp : npreds) {
            if (needComma) {pred += ", ";}
            pred += vp.toString();
            pred += "}";
            needComma = true;
        }
        pred += "}";

        return pred;
    }

    public Set getNodeToIndexKeySet() {
        return nodeToIndex.keySet();
    }

    public Set getIndexToNodeKeySet() {
        return indexToNode.keySet();
    }
}
