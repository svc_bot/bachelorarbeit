package simple;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import common.InstructionType;
import soot.*;
import soot.jimple.Constant;
import soot.jimple.internal.JAssignStmt;
import soot.shimple.PhiExpr;
import soot.shimple.toolkits.scalar.ShimpleLocalDefs;
import soot.shimple.toolkits.scalar.ShimpleLocalUses;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.UnitValueBoxPair;

import static common.InstructionType.*;

@Deprecated  // because UnitGraph is not enough for this analysis
public class SparseAnalysisOld<N> {
	protected DirectedGraph<N> graph;
	private ShimpleLocalDefs localDefs;	// Nodes definition points
	private ShimpleLocalUses localUses;	// Nodes usage points
	private Map<N, Integer> nodeToIndex = new HashMap<N, Integer>(); // contains indexes of every node
	private Map<Integer, N> indexToNode = new HashMap<Integer, N>(); // contains reference to every node by its index
	private HashMap<Integer, BitSet> predicatesWithoutInstruction = new HashMap<Integer, BitSet>();
	private int step = 0; // reflects how many steps the analysis needs. used for logging & debuging
	public  Map<Integer, HashSet<VarPredicate> > pathPredicates = new HashMap<Integer, HashSet<VarPredicate> >();
	
	public SparseAnalysisOld(DirectedGraph<N> graph, ShimpleLocalDefs localDefs,
	                         ShimpleLocalUses localUses) {
		this.graph = graph;
		this.localDefs = localDefs;
		this.localUses = localUses;
		G.v().out.println();
		setIndexes();
		generateKillSets();
		
		doAnalysis();
	}

    /**
     * Version of Analysis without path designators
     */
	private void doAnalysis() {

		List<N> worklist = new ArrayList<N>();
		
		// add all nodes to the worklist and show instruction in console
		for (N n : graph) {
			worklist.add(n);
			G.v().out.println(nodeToIndex.get(n) + " " + n.toString() + " \t type: " + n.getClass());
		}
		
		// initialize pathPredicates
		for (int i = 0; i < graph.size(); i++) {
			pathPredicates.put(i, new HashSet<VarPredicate>());
		}
		
		G.v().out.println();
		G.v().out.println("Doing analysis without path designators: ");
		G.v().out.println();

		while (!worklist.isEmpty()){
			N nextNode = worklist.remove(0); // pop first element from the list and use it later			
			HashSet<VarPredicate> newPred = computePredOf(nextNode);
			HashSet<VarPredicate> oldPred = pathPredicates.get(nodeToIndex.get(nextNode));
			
			if(!newPred.equals(oldPred)){
				//G.v().out.println(nodeToIndex.get(nextNode) + " " + oldPred + " -> " + newPred + " type: " + getType(nextNode));
				pathPredicates.put(nodeToIndex.get(nextNode), newPred);
				List<UnitValueBoxPair> updateQueue = localUses.getUsesOf((Unit)nextNode);
				if(updateQueue == null) continue; // nothing to do, skip to next instruction
				//G.v().out.println("UpdateQueue: " + updateQueue);
				for (UnitValueBoxPair uvbox : updateQueue) {
					Unit queuedUnit = uvbox.getUnit();
					if(!worklist.contains(queuedUnit)) {
						worklist.add((N) queuedUnit);
					}
				}	
			}
			//log(nextNode);
            step++;
			
		}
		
		G.v().out.println("Done.");
		G.v().out.println("Check the results: ");
		
		for (N n : graph) {
			if (getType(n) != none){
				G.v().out.println(nodeToIndex.get(n) + " -> " + getPredicateString(n) + " type: " + getType(n));
			}
		}
	}

	/**
     * This method fills nodeToIndex and indexToNode sets with appropriate values
     */
    private void setIndexes(){
        int index = 0;

        for(N n : graph) {
            nodeToIndex.put(n, index);
            indexToNode.put(index, n);
            ++index;
        }
    }

    /**
     * This method generates for every node of not "none"-type a bitset, which is a kill set of that node
     */
	void generateKillSets() {
		HashSet<N> allPredicates = new HashSet<N>();
		// fill allPreds
		for (N node : graph) { if (getType(node) != none) allPredicates.add(node); }
		//
		for (N node : graph) {
			if (getType(node) != none) {
				BitSet predicatesWithoutNode = new BitSet(graph.size());
				for (N otherNode : allPredicates) {
					if (node != otherNode && getType(otherNode) != none) {
						// get all uses of otherNode
						Value rhs = ((JAssignStmt) otherNode).getRightOp();
						List<Value> uses = new java.util.LinkedList<Value>();
						if (rhs instanceof Local) {
							uses.add(rhs);
						} else {
							for (Object valueBox : rhs.getUseBoxes()) {
								uses.add(((ValueBox) valueBox).getValue());
							}
						}
						//System.out.println(node + " vs " + otherNode + " -> " + ((JAssignStmt) otherNode).getRightOp() + "/"+  uses);
						ArrayList<N> operands = new ArrayList<N>();
						for (Value use : uses) {
							if (use instanceof Constant) continue;
							N useNode = (N) localDefs.getDefsOf((Local) use).get(0);
							operands.add(useNode);
						}
						if (!operands.contains(node)) {
							predicatesWithoutNode.set(nodeToIndex.get(otherNode));
						}
					}
				}
				predicatesWithoutInstruction.put(nodeToIndex.get(node), predicatesWithoutNode);
			}
		}
	}

    /**
     *
     * @param n is node for which killset of phi function extras will be computed
     * @return HashSet containing strings which represent phi-function extras which should be removed
     */
	private HashSet<String> getPhiFuncExtrasKillset(N n){
        HashSet<String> result = new HashSet<>();
        HashSet<N> allPredicates = new HashSet<N>();
        // fill allPreds
        for (N node : graph) { if (getType(node) != none) allPredicates.add(node); }

        return result;
    }

	/**
	 * writes analysis log to console
	 * @param nextNode is a Graph node predicate of which is currently being computed in the analysis
	 */
	private void log(N nextNode) {
		//analysis log
		if (getType(nextNode) != none){
			G.v().out.println();
			G.v().out.println("Step: " + step);

			for (N n : graph) {
				if (getType(n) != none){
                    if (n.equals(nextNode)){
                        G.v().out.println("node: " + nodeToIndex.get(n) + " predicates: " + pathPredicates.get(nodeToIndex.get(n)) + " <<< new");
                    } else G.v().out.println("node: " + nodeToIndex.get(n) + " predicates: " + pathPredicates.get(nodeToIndex.get(n)));

				}
			}
		}
    }

    private InstructionType getType(N node){
        if (node instanceof JAssignStmt) {
            if(((JAssignStmt) node).getRightOp() instanceof PhiExpr) return phiFunction;
            if(((JAssignStmt) node).getRightOp() instanceof Constant)return constAssign;
            else {
                if ( ((JAssignStmt) node).getRightOp() instanceof Local){
                    return varAssign;
                }
                else {
                    return complexAssign;
                }
            }
        }
        return none;
    }

	private HashSet<VarPredicate> computePredOf(N node){
		BitSet predsWithoutx = predicatesWithoutInstruction.get(nodeToIndex.get(node));
		HashSet<VarPredicate> ypreds 	   	= new HashSet<>();
		HashSet<VarPredicate> zpreds 	    = new HashSet<>();
		HashSet<VarPredicate> computedPred  = new HashSet<>();

		switch (getType(node)) {
		case constAssign:
			BitSet bitSet = new BitSet(graph.size());
			bitSet.set(nodeToIndex.get(node));
			computedPred.add(new VarPredicate(bitSet));
			break;

		case varAssign:
			// x = y,
			// preds = xpreds ∩ ypreds ∪ {x=y}
			Value v = ((Unit)node).getUseBoxes().get(0).getValue(); // getUseBoxes returns a list containing only one element, which itself is another list
			List<N> listDefs = (List<N>) localDefs.getDefsOf((Local) v); // in this case should always have max 1 element
			ypreds = pathPredicates.get(nodeToIndex.get(listDefs.get(0))); // get predicates of y
			for(VarPredicate varPredicate : ypreds){
                VarPredicate newPredicate = varPredicate.copy();
				newPredicate.getPredicate().and(predsWithoutx);
                newPredicate.getPredicate().set(nodeToIndex.get(node));
				computedPred.add(newPredicate);
			}
			break;

		case complexAssign:
			// x = y + z, z can be constant or variable
			List<ValueBox> rightOps = ((Unit) node).getUseBoxes(); // list contains value boxes of y and z

			// In Shimple there can be max 2 values
			if (!(rightOps.get(0).getValue() instanceof Constant)){
				N yOperand = (N) localDefs.getDefsOf((Local) rightOps.get(0).getValue()).get(0);
				ypreds = pathPredicates.get(nodeToIndex.get(yOperand));
			}
			if (!(rightOps.get(1).getValue() instanceof Constant)){
				N zOperand = (N) localDefs.getDefsOf((Local) rightOps.get(1).getValue()).get(0);
				zpreds = pathPredicates.get(nodeToIndex.get(zOperand));
			}
			// union over preds of y and z
			if (ypreds.isEmpty()){
				for (VarPredicate zVarPredicate : zpreds){
                    VarPredicate newPredicate = zVarPredicate.copy();
                    newPredicate.getPredicate().and(predsWithoutx);
                    newPredicate.getPredicate().set(nodeToIndex.get(node));
					computedPred.add(newPredicate);
				}
			} else if(zpreds.isEmpty()){
				for (VarPredicate yVarPredicate : ypreds){
                    VarPredicate newPredicate = yVarPredicate.copy();
                    newPredicate.getPredicate().and(predsWithoutx);
                    newPredicate.getPredicate().set(nodeToIndex.get(node));
                    computedPred.add(newPredicate);
				}
			} else {
				for (VarPredicate yVarPredicate : ypreds) {
					for (VarPredicate zVarPredicate : zpreds){
                        VarPredicate newPredicate = yVarPredicate.copy();
                        // compute new predicate BitSet
                        newPredicate.getPredicate().or(zVarPredicate.getPredicate());
                        newPredicate.getPredicate().and(predsWithoutx);
						newPredicate.getPredicate().set(nodeToIndex.get(node));
                        //compute new phiFuncExtras
                        newPredicate.getPhiFuncExtras().addAll(zVarPredicate.getPhiFuncExtras());
						computedPred.add(newPredicate);
					}
				}
			}

			break;

		case phiFunction:

			// TODO: currently, only the Phi-function self is addressed, not all the Phi-functions defined in the node

			// get everything needed for computation
			//--------------------------------------
			List<ValueBox> operandsUseBoxes = ((JAssignStmt) node).getRightOp().getUseBoxes(); // get list of values used inside of the definition of the node
            ArrayList<N> uses = new ArrayList<>();
            BitSet killSet = new BitSet(graph.size());
            killSet.flip(0, graph.size());
            killSet.and(predicatesWithoutInstruction.get(nodeToIndex.get(node)));
			for (ValueBox useBox : operandsUseBoxes){
				N operandsNode = (N) localDefs.getDefsOf((Local) useBox.getValue()).get(0);
                uses.add(operandsNode);
                if (getType(operandsNode) == InstructionType.phiFunction)
                    killSet.and(predicatesWithoutInstruction.get(nodeToIndex.get(operandsNode)));
			}

            for (N operand : uses) {
				for (VarPredicate varPredicate : pathPredicates.get(nodeToIndex.get(operand))) {
                    VarPredicate newPredicate = varPredicate.copy();
                    newPredicate.getPredicate().and(killSet);
                    newPredicate.getPhiFuncExtras()
                            .add(((Unit) node).getDefBoxes().get(0).getValue() + " = " + ((Unit) operand).getDefBoxes().get(0).getValue());
                    computedPred.add(newPredicate);
				}
			}

			break;

		case assertion:
			break;

		default:
			break;
		}

		return computedPred;
	}

    /**
     *
     * @param node
     * @return String representation of the node predicates
     */
    private String getPredicateString(N node){
        String pred = "{";
        HashSet<VarPredicate> npreds = pathPredicates.get(nodeToIndex.get(node));
        boolean needComma = false;
        for (VarPredicate vp : npreds) {
            if (needComma) {pred += ", ";}
            pred += vp.toString();
            pred += "}";
            needComma = true;
        }
        pred += "}";

        return pred;
    }

}
