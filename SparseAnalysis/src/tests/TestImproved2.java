package tests;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;

import improved.ImprovedSparseAnalysis;
import improved.PathDesignator;
import improved.ImprovedVarPredicate;
import org.junit.*;

import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.Unit;
import soot.options.Options;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.shimple.ShimpleBody;
import soot.shimple.toolkits.scalar.*;

// TestCase 2 Medium
public class TestImproved2 {

	// setup for test case
	public static void main(String[] args) {
		PackManager.v().getPack("stp").add(
				new Transform("stp.myTransform", new BodyTransformer() {
					protected void internalTransform(Body body, String phase, Map options) {
						ImprovedSparseAnalysis analysis = new ImprovedSparseAnalysis(new ExceptionalBlockGraph(body),
								new ShimpleLocalDefs((ShimpleBody) body),
								new ShimpleLocalUses((ShimpleBody) body));
//						Map <Integer, HashSet<ImprovedVarPredicate>> result = analysis.pathPredicates;
//						Map <Integer, HashSet<ImprovedVarPredicate>> expected = getExpected();
//						if (body.getMethod().isEntryMethod()) Assert.assertEquals(expected, result);
					}					
				}));
//		String[] test = {	"-cp", "/home/alex/Dokumente/Bachelorarbeit//soot-2.5.0.jar",
//							"-cp",
//							"/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/lib/rt.jar:/home/alex/workspace/bachelorarbeit/ProjectToTest/bin/",
//							"TestImproved2"};
//		args = test;
		Options.v().full_resolver();
		Options.v().set_via_shimple(true);
		soot.Main.main(args);
	}
	
//	private static Map <Integer, HashSet<ImprovedVarPredicate>> getExpected(){
//		Map<Integer, HashSet<ImprovedVarPredicate>> expected 	= new HashMap<Integer, HashSet<ImprovedVarPredicate>>();
//		String separateHS  = "[#]"; // HashSets are separated with #
//		String separateVP  = "[u]"; // VarPredicates are separated with u
//		String separatePD  = "[;]"; // Pathdesignators are separeted from Bitset with ;
//
//		String expectedStr ="{(-1,-1);{}}#" +
//							"{(-1,-1);{1}}#" +
//							"{(-1,-1);{1,2}}#" +
//							"{(-1,-1);{3}}#" +
//							"{(-1,-1);{3,4}}#" +
//							"{(-1,-1);{1,2,5}}#" +
//							"{(-1,-1);{1,2,3,4,5,6}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{1,2,10}}#" +
//							"{(-1,-1);{1,2,10,11}}#" +
//							"{(-1,-1);{1,2,10,11,12}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{3,4,15}}#" +
//							"{(-1,-1);{3,4,15,16}}#" +
//							"{(-1,-1);{3,4,15,16,17}}#" +
//							"{(4,0);{3,4}}u{(12,1);{1,2,10,11,12}}#" +
//							"{(2,1);{1,2}}u{(17,0);{3,4,15,16,17}}#" +
//							"{(-1,-1);{}}#" +
//							"{(-1,-1);{}}#";
//
//		String[] hashsetTokens = expectedStr.split(separateHS);
//		for (int i = 0; i < hashsetTokens.length; i++) {
//			HashSet<ImprovedVarPredicate> tmp = new HashSet<ImprovedVarPredicate>();
//			String[] varpredTokens = hashsetTokens[i].split(separateVP);
//			for (int j = 0; j < varpredTokens.length; j++) {
//				String[] designatorBitsetPair = varpredTokens[j].split(separatePD);
//				HashSet<PathDesignator> setDesignator = parseDesignator(designatorBitsetPair[0]);
//				BitSet bitset = makeBS(designatorBitsetPair[1]);
//				tmp.add(new ImprovedVarPredicate(setDesignator, bitset));
//			}
//			expected.put(i, tmp);
//		}
//		return expected;
//	}
//	private static BitSet makeBS(String bitsetStr){
//		//G.v().out.println(bitsetStr);
//		BitSet bs = new BitSet(21); // hard code is evil!
//		String separator = "[,]+";
//		bitsetStr = bitsetStr.replace("{", "");
//		bitsetStr = bitsetStr.replace("}", "");
//		if (bitsetStr.equals("")) return bs;
//		else {
//			String[] bits = bitsetStr.split(separator);
//			if(Integer.parseInt(bits[0]) == -1) return bs;
//			for (int i = 0; i < bits.length; ++i) {
//				int bitIndex = Integer.parseInt(bits[i]);
//				bs.set(bitIndex);
//			}
//			return bs;
//		}
//	}
//	private static HashSet<PathDesignator> parseDesignator(String setDesignatorsString){
//		HashSet<PathDesignator> result = new HashSet<PathDesignator>();
//		String [] designatorStr = setDesignatorsString.split("[),(]");
//		for (String designator : designatorStr) {
//			designator = designator.replace("{", "");
//			designator = designator.replace("}", "");
//			designator = designator.replace("(", "(");
//			designator = designator.replace(")", ")");
//			String [] desValuePair = designator.split("[,]");
//			if (desValuePair.length < 2){
//				result.add(new PathDesignator());
//			} else {
//				int predeccesor = Integer.parseInt(desValuePair[0]);
//				int pathIndex = Integer.parseInt(desValuePair[1]);
//				result.add(new PathDesignator(predeccesor, pathIndex));
//			}
//		}
//		return result;
//	}
	
}