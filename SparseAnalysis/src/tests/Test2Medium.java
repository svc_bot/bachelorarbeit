package tests;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;

import simple.SparseAnalysisOld;
import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.Unit;
import soot.options.Options;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.shimple.ShimpleBody;
import soot.shimple.toolkits.scalar.*;

// TestCase 2 Medium
public class Test2Medium {
	private static String progToTest = "Prog2Medium";
	// setup for test case
	public static void main(String[] args) {
		PackManager.v().getPack("stp").add(
				new Transform("stp.myTransform", new BodyTransformer() {
					protected void internalTransform(Body body, String phase, Map options) {
						SparseAnalysisOld<Unit> analysis = new SparseAnalysisOld<Unit>(new ExceptionalUnitGraph(body),
								new ShimpleLocalDefs((ShimpleBody) body),
								new ShimpleLocalUses((ShimpleBody) body));
//						Map <Integer, HashSet<BitSet>> result = analysis.pathPredicates;
//						Map <Integer, HashSet<BitSet>> expected = getExpected();
//						if (body.getMethod().isEntryMethod()) Assert.assertEquals(expected, result);
					}					
				}));
		args = new String[]{"--d",
				"/home/alex/workspace/bachelorarbeit/ProjectToTest/sootOutput",
				"--cp",
				"/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/home/alex/workspace/bachelorarbeit/ProjectToTest/bin/:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar::/home/alex/workspace/bachelorarbeit/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar",
				"--xml-attributes",
				"--src-prec",
				"java",
				progToTest};
		Options.v().full_resolver();
		Options.v().set_via_shimple(true);
		soot.Main.main(args);
	}
	
	private static Map <Integer, HashSet<BitSet>> getExpected(){
		Map<Integer, HashSet<BitSet>> expected 	= new HashMap<Integer, HashSet<BitSet>>();
		String separateHS  = "[#]"; // HashSets are separated with #
		String separateBS  = "[u]"; // HashSets are separated with u
		
		String expectedStr = "{{}}#" +
							 "{{1}}#" +
							 "{{1,2}}#" +
							 "{{3}}#" +
							 "{{3,4}}#" +
							 "{{1,2,5}}#" +
							 "{{1,2,3,4,5,6}}#" +
							 "{{}}#" +
							 "{{}}#" +
							 "{{}}#" +
							 "{{1,2,10}}#" +
							 "{{1,2,10,11}}#" +
						 	 "{{1,2,10,11,12}}#" +
							 "{{}}#" +
							 "{{}}#" +
							 "{{3,4,15}}#" +
							 "{{3,4,15,16}}#" +
							 "{{3,4,15,16,17}}#" +
							 "{{1,2,10,11,12}}u{{3,4}}#" +
							 "{{1,2}}u{{3,4,15,16,17}}#" +
							 "{{}}#" +
							 "{{}}#";
		
		String[] hashsetTokens = expectedStr.split(separateHS);
		for (int i = 0; i < hashsetTokens.length; i++) {
			HashSet<BitSet> tmp = new HashSet<BitSet>();
			String[] bitsetTokens = hashsetTokens[i].split(separateBS);
			for (int j = 0; j < bitsetTokens.length; j++) {
				tmp.add(makeBS(bitsetTokens[j]));
			}
			expected.put(i, tmp);
		}
		return expected;
	}
	private static BitSet makeBS(String bitsetStr){
		//G.v().out.println(bitsetStr);
		BitSet bs = new BitSet(21); // hard code is evil!
		String separator = "[,]+";
		bitsetStr = bitsetStr.replace("{", "");
		bitsetStr = bitsetStr.replace("}", "");	
		if (bitsetStr.equals("")) return bs;
		else {			
			String[] bits = bitsetStr.split(separator);
			if(Integer.parseInt(bits[0]) == -1) return bs;
			for (int i = 0; i < bits.length; ++i) {
				int bitIndex = Integer.parseInt(bits[i]);
				bs.set(bitIndex);
			}			
			return bs;
		}
	}
	
}