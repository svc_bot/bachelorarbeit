package tests;

import java.util.Map;

import simple.SparseAnalysisOld;
import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.Unit;
import soot.options.Options;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.shimple.ShimpleBody;
import soot.shimple.toolkits.scalar.*;

// TestCase 4
public class Test5 {

	private static String progToTest = "Prog5";

	// setup for test case
	public static void main(String[] args) {
		PackManager.v().getPack("stp").add(
				new Transform("stp.myTransform", new BodyTransformer() {
					protected void internalTransform(Body body, String phase, Map options) {
						SparseAnalysisOld<Unit> analysis = new SparseAnalysisOld<Unit>(new ExceptionalUnitGraph(body),
								new ShimpleLocalDefs((ShimpleBody) body),
								new ShimpleLocalUses((ShimpleBody) body));
//						Map <Integer, HashSet<BitSet>> result = analysis.pathPredicates;
//						Map <Integer, HashSet<BitSet>> expected = getExpected();
						//if (body.getMethod().isEntryMethod()) Assert.assertEquals(expected, result);
					}					
				}));
		args = new String[]{"--d",
				"/home/alex/workspace/bachelorarbeit/ProjectToTest/sootOutput",
				"--cp",
				"/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/home/alex/workspace/bachelorarbeit/ProjectToTest/bin/:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar::/home/alex/workspace/bachelorarbeit/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar",
				"--xml-attributes",
				"--src-prec",
				"java",
				progToTest};
		Options.v().full_resolver();
		Options.v().set_via_shimple(true);
		soot.Main.main(args);
	}
//
//	private static Map <Integer, HashSet<BitSet>> getExpected(){
//		Map<Integer, HashSet<BitSet>> expected 	= new HashMap<Integer, HashSet<BitSet>>();
//		String separateHS  = "[#]"; // HashSets are separated with #
//		String separateBS  = "[u]"; // BitSets are separated with u
//
//		String expectedStr = "{{}}#" +
//							 "{{1}}#" +
//							 "{{1}u{1,9,10}}#" +
//							 "{{}}#" +
//							 "{{}}#" +
//							 "{{}}#" +
//							 "{{}}}#" +
//							 "{{}}#" +
//							 "{{1,8}u{1,8,10}}#" +
//							 "{{1,8,9}}#" +
//							 "{{1,8,9,10}}#" +
//							 "{{}}#" +
//						 	 "{{}}#" +
//							 "{{}}#";
//
//		String[] hashsetStr = expectedStr.split(separateHS);
//		for (int i = 0; i < hashsetStr.length; i++) {
//			HashSet<BitSet> tmp = new HashSet<BitSet>();
//			String[] bitsetStr = hashsetStr[i].split(separateBS);
//			for (int j = 0; j < bitsetStr.length; j++) {
//				tmp.add(makeBS(bitsetStr[j]));
//			}
//			expected.put(i, tmp);
//		}
//		return expected;
//	}
//	private static BitSet makeBS(String str){
//		BitSet bs = new BitSet(13); // hard code is evil!
//		String separator = "[,]";
//		str = str.replace("{", "");
//		str = str.replace("}", "");
//		if (str.equals("")) return bs;
//		else {
//			String[] bits = str.split(separator);
//
//			for (int i = 0; i < bits.length; ++i) {
//				int bitIndex = Integer.parseInt(bits[i]);
//				bs.set(bitIndex);
//			}
//
//			return bs;
//		}
//	}
	
}