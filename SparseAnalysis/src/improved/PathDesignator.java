package improved;

public class PathDesignator implements Cloneable{
	private int predecessorIndex;
	private int pathIndex;
	
	/**
	 * use this to define an empty path designator inside of a variable predicate
	 */
	public PathDesignator() {
		predecessorIndex = -1;
		pathIndex = -1;
	}

	public PathDesignator(int predecessorIndex, int pathIndex) {
		this.predecessorIndex = predecessorIndex;
		this.pathIndex = pathIndex;
	}

	protected int getPredecessorIndex() {
		return predecessorIndex;
	}

	protected void setPredecessorIndex(int predecessorIndex) {
		this.predecessorIndex = predecessorIndex;
	}

	protected int getPathIndex() {
		return pathIndex;
	}

	protected void setPathIndex(int pathIndex) {
		this.pathIndex = pathIndex;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pathIndex;
		result = prime * result + predecessorIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PathDesignator other = (PathDesignator) obj;
		if (pathIndex != other.pathIndex)
			return false;
		if (predecessorIndex != other.predecessorIndex)
			return false;
		return true;
	}

	public PathDesignator copy(){
		return new PathDesignator(predecessorIndex, pathIndex);
	}

    @Override
    protected Object clone() {
        return this.copy();
    }

    @Override
	public String toString() {
		return "(" + predecessorIndex + ", " + pathIndex + ")";
	}
	
}
