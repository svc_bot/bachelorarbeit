package improved;

import simple.VarPredicate;

import java.util.BitSet;
import java.util.HashSet;

/**
 * 
 * This class contains designator-bitset-pair
 * if designator is equal to -1, that means that the variable has 
 * no path designator. 
 * (this should prevent unnecessary null pointer references and checks)
 *
 */
public class ImprovedVarPredicate extends VarPredicate {
	private HashSet<PathDesignator> designatorSet = new HashSet<>();

    public ImprovedVarPredicate(HashSet<PathDesignator> designatorSet, HashSet<String> phiFuncExtras, BitSet predicate) {
        super(predicate, phiFuncExtras);
        this.designatorSet = designatorSet;
    }

    public ImprovedVarPredicate(BitSet predBitSet) {
        super(predBitSet);
        this.designatorSet.add(new PathDesignator());
    }

    public ImprovedVarPredicate(ImprovedVarPredicate improvedVarPredicate){
        super(improvedVarPredicate.getPredicate(), improvedVarPredicate.getPhiFuncExtras());
        for (PathDesignator p : improvedVarPredicate.designatorSet){
            this.designatorSet.add(p.copy());
        }
    }

    public HashSet<PathDesignator> getDesignatorSet() {
        return designatorSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImprovedVarPredicate)) return false;

        ImprovedVarPredicate that = (ImprovedVarPredicate) o;

        if (!designatorSet.equals(that.designatorSet)) return false;
        if (!getPhiFuncExtras().equals(that.getPhiFuncExtras())) return false;
        return getPredicate().equals(that.getPredicate());
    }

    @Override
    public int hashCode() {
        int result = designatorSet.hashCode();
        result = 31 * result + getPhiFuncExtras().hashCode();
        result = 31 * result + getPredicate().hashCode();
        return result;
    }

    public ImprovedVarPredicate copy(){
		return new ImprovedVarPredicate(this);
	}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean needComma = false;
        sb.append("[");
        sb.append("designator={");
        for (PathDesignator designator : designatorSet){
            if (needComma) {sb.append(", ");}
            sb.append(designator);
            needComma = true;
        }
        sb.append("}, ");
        needComma = false;
        sb.append("predicate=");
        sb.append("{");
        for (int i = 0; i < getPredicate().length(); i++) {
            if (getPredicate().get(i)) {
                if (needComma) {sb.append(", "); }
                sb.append(i);
                needComma = true;
            }
        }
        if (getPhiFuncExtras().size() > 0) {
            for (String str : getPhiFuncExtras()){
                if (needComma) {sb.append(", "); }
                sb.append(str);
                needComma = true;
            }
        }
        sb.append("}]");

        return sb.toString();
    }

}
