package improved;

import java.util.*;

import simple.VarPredicate;
import soot.*;
import soot.jimple.Constant;
import soot.jimple.internal.JAssignStmt;
import soot.shimple.toolkits.scalar.ShimpleLocalDefs;
import soot.shimple.toolkits.scalar.ShimpleLocalUses;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.DirectedGraph;

public class ImprovedSparseAnalysis extends simple.SparseAnalysis{
    //public Map<Integer, HashSet<ImprovedVarPredicate>> pathPredicates;

    public ImprovedSparseAnalysis(DirectedGraph<Block> graph, ShimpleLocalDefs localDefs, ShimpleLocalUses localUses) {
//        this.graph = graph;
//        this.localDefs = localDefs;
//        this.localUses = localUses;
//        this.nodesToAnalyse = new ArrayList<>(); // we want to operate on nodes of local type
//        indexToNode = new HashMap<Integer, Unit>();
//        nodeToIndex = new HashMap<Unit, Integer>();
//        killSet = new HashMap<Integer, BitSet>();
//        killPhiExtras = new HashMap<Integer, HashSet<String>>();
//        pathPredicates = new HashMap<Integer, HashSet<ImprovedVarPredicate> >();
//        nodeToBlockIndex = new int[graph.iterator().next().getBody().getUnits().size()]; // beware, .getBody() returns the body of whole graph, not of a single block
//        setUpIndexesAndFields();
//        genereteKillsets();
//        doAnalysis();
        super(graph, localDefs, localUses);
    }

//    protected void setUpIndexesAndFields() {
//        int nodeIndex   = 0;
//        int blockIndex  = 0;
//
//        for (Block block : graph){
//            Iterator<Unit> blockIterator = block.iterator();
//            while (blockIterator.hasNext()) {
//                Unit node = blockIterator.next();
//                if (getType(node) != none) {
//                    nodesToAnalyse.add(node);
//                    indexToNode.put(nodeIndex, node);
//                    nodeToIndex.put(node, nodeIndex);
//                    nodeToBlockIndex[nodeIndex] = blockIndex;
//                    ++nodeIndex;
//                }
//            }
//            ++blockIndex;
//        }
//
//        for (int i = 0; i < nodesToAnalyse.size(); i++) {
//            killPhiExtras.put(i, new HashSet<String>());
//        }
//
//        for (int i = 0; i < nodesToAnalyse.size(); i++) {
//            pathPredicates.put(i, new HashSet<VarPredicate>());
//        }
//    }

//    public void doAnalysis() {
//        // output instructions to console + sanity check
//        // block view
//        for (Block block : graph) {
//            System.out.println(block);
//        }
//        // writes instructions to console
//        System.out.println("Nodes to analyse: ");
//        for (int i : indexToNode.keySet()){
//            System.out.println(i + ": " + indexToNode.get(i) + " in block " + nodeToBlockIndex[i]);
//        }
//
//        List<Unit> worklist = new ArrayList<>();
//        worklist.addAll(nodesToAnalyse);
//
//        G.v().out.println();
//        G.v().out.println("Doing analysis without path designators: ");
//        G.v().out.println();
//
//        while (!worklist.isEmpty()){
//            Unit nextNode = worklist.remove(0); // pop first element from the list and use it later
//            HashSet<VarPredicate> newPred = computePredOf(nextNode);
//            HashSet<VarPredicate> oldPred = pathPredicates.get(nodeToIndex.get(nextNode));
//
//            if(!newPred.equals(oldPred)){
//                pathPredicates.put(nodeToIndex.get(nextNode), newPred);
//                List<UnitValueBoxPair> updateQueue = localUses.getUsesOf(nextNode);
//                if(updateQueue == null) continue; // nothing to do, skip to next instruction
//                for (UnitValueBoxPair uvbox : updateQueue) {
//                    Unit queuedUnit = uvbox.getUnit();
//                    if(!worklist.contains(queuedUnit)) {
//                        worklist.add(queuedUnit);
//                    }
//                }
//            }
//            ++step;
//        }
//
//        G.v().out.println("Done.");
//        G.v().out.println("Check the results!");
//        G.v().out.println("computed path predicates: ");
//
//        for (int i : indexToNode.keySet()) {
//            System.out.println(i + ": " + indexToNode.get(i) + " -> " + pathPredicates.get(i));
//        }
//    }

    private HashSet<VarPredicate> computePredOf(Unit node) {
        HashSet<VarPredicate> computedPred  = new HashSet<>();
        HashSet<VarPredicate> ypreds        = new HashSet<>();
        HashSet<VarPredicate> zpreds        = new HashSet<>();

        switch (getType(node)) {

            case constAssign:
                BitSet bitSet = new BitSet(nodesToAnalyse.size());
                bitSet.set(nodeToIndex.get(node));
                computedPred.add(new ImprovedVarPredicate(bitSet));
                break;
            case varAssign:
                Value v = node.getUseBoxes().get(0).getValue(); // getUseBoxes returns a list containing only one element, which itself is another list
                List<Unit> listDefs = localDefs.getDefsOf((Local) v); // in this case should always have max 1 element
                ypreds = pathPredicates.get(nodeToIndex.get(listDefs.get(0))); // get predicates of y
                if (!listDefs.isEmpty() && ypreds == null) {
                    // this happens on virtual and special invokes
                    BitSet bitSetSpecialCase = new BitSet(nodesToAnalyse.size());
                    bitSetSpecialCase.set(nodeToIndex.get(node));
                    computedPred.add(new ImprovedVarPredicate(bitSetSpecialCase));
                } else {
                    for(VarPredicate varPredicate : ypreds){
                        VarPredicate newPredicate = varPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                }
                break;
            case complexAssign:
                // x = y + z, y or z (exclusevly) can be constant
                List<ValueBox> rightOps = node.getUseBoxes(); // list contains value boxes of y and z

                // In Shimple there can be max 2 values
                if (!(rightOps.get(0).getValue() instanceof Constant) && (rightOps.get(0).getValue() instanceof Local)){
                    Unit yOperand = localDefs.getDefsOf((Local) rightOps.get(0).getValue()).get(0);
                    ypreds = pathPredicates.get(nodeToIndex.get(yOperand));
                }
                if (!(rightOps.get(1).getValue() instanceof Constant) && (rightOps.get(1).getValue() instanceof Local)){
                    Unit zOperand = localDefs.getDefsOf((Local) rightOps.get(1).getValue()).get(0);
                    zpreds = pathPredicates.get(nodeToIndex.get(zOperand));
                }
                // union over preds of y and z
                if (ypreds.isEmpty()){
                    for (VarPredicate zVarPredicate : zpreds){
                        VarPredicate newPredicate = zVarPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                } else if(zpreds.isEmpty()){
                    for (VarPredicate yVarPredicate : ypreds){
                        VarPredicate newPredicate = yVarPredicate.copy();
                        newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                        newPredicate.getPredicate().set(nodeToIndex.get(node));
                        newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        computedPred.add(newPredicate);
                    }
                } else {
                    for (VarPredicate yVarPredicate : ypreds) {
                        for (VarPredicate zVarPredicate : zpreds){
                            if (!isDefinite(yVarPredicate.getDesignatorSet(), zVarPredicate.getDesignatorSet())) continue;
                            else {
                                VarPredicate newPredicate = yVarPredicate.copy();
                                // compute new predicate BitSet
                                newPredicate.getPredicate().or(zVarPredicate.getPredicate());
                                newPredicate.getPredicate().and(killSet.get(nodeToIndex.get(node)));
                                newPredicate.getPredicate().set(nodeToIndex.get(node));
                                // compute new phiFuncExtras
                                newPredicate.getPhiFuncExtras().addAll(zVarPredicate.getPhiFuncExtras());
                                newPredicate.getPhiFuncExtras().removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                                // compute new path designator
                                newPredicate.getDesignatorSet().addAll(zVarPredicate.copy().getDesignatorSet());
                                computedPred.add(newPredicate);
                            }
                        }
                    }
                }
                break;
            case assertion:
                break;
            case phiFunction:
                // get everything needed for computation
                //--------------------------------------
                List<ValueBox> operandsUseBoxes = ((JAssignStmt) node).getRightOp().getUseBoxes(); // get list of values used inside of the definition of the node
                ArrayList<Unit> uses = new ArrayList<>();
                BitSet killSetIntersection = getPhiNodeKillsetsIntersection(node);
                for (ValueBox useBox : operandsUseBoxes){
                    Unit operandsNode = localDefs.getDefsOf((Local) useBox.getValue()).get(0);
                    uses.add(operandsNode);
                }
                int pathIndex = 0;
                for (Unit operand : uses) {
                    for (VarPredicate varPredicate : pathPredicates.get(nodeToIndex.get(operand))) {
                        BitSet newPredicate = (BitSet) varPredicate.getPredicate().clone();
                        HashSet<String> newPhiFunctionExtras = (HashSet<String>) varPredicate.getPhiFuncExtras().clone();
                        HashSet<PathDesignator> newPathDesignators;
                        // compute new predicate set
                        newPredicate.and(killSetIntersection);
                        // compute new phi function extras
                        newPhiFunctionExtras.removeAll(killPhiExtras.get(nodeToIndex.get(node)));
                        newPhiFunctionExtras.add(node.getDefBoxes().get(0).getValue() + " = " + operand.getDefBoxes().get(0).getValue());
                        // compute new path designators
                        newPathDesignators = override(varPredicate.getDesignatorSet(), new PathDesignator(nodeToIndex.get(node), pathIndex));
                        computedPred.add(new ImprovedVarPredicate(newPathDesignators, newPhiFunctionExtras, newPredicate));
                    }
                    ++pathIndex;
                }
                break;
            case none:
                break;
        }

        return computedPred;
    }

    private HashSet<PathDesignator> override(HashSet<PathDesignator> oldDesignators, PathDesignator newDesignator) {
        HashSet<PathDesignator> result = new HashSet<PathDesignator>();
        HashSet<PathDesignator> newPathDesignators = (HashSet<PathDesignator>) oldDesignators.clone();
        for (PathDesignator oldPD : oldDesignators){
            if (oldPD.getPredecessorIndex() == newDesignator.getPredecessorIndex()){
                if (oldPD.getPathIndex() != newDesignator.getPathIndex()) newPathDesignators.remove(oldPD);
            }
        }
        newPathDesignators.add(newDesignator);
        result = (HashSet<PathDesignator>) newPathDesignators.clone();
        if (newPathDesignators.size() > 1){
            for (PathDesignator pd : newPathDesignators){
                if (pd.getPredecessorIndex() == -1 && pd.getPathIndex() == -1) result.remove(pd);
            }
        }
        return result;
    }

    private boolean isDefinite(HashSet<PathDesignator> delta, HashSet<PathDesignator> gamma) {
        for (PathDesignator d : delta){
            for (PathDesignator g : gamma){
                if (nodeToBlockIndex[d.getPredecessorIndex()] == nodeToBlockIndex[g.getPredecessorIndex()])
                    if (d.getPathIndex() != g.getPathIndex()) return false;
            }
        }
        return true;
    }

    @Override
    protected String getPredicateString(Unit node){
        String pred = "{";
        HashSet<VarPredicate> npreds = pathPredicates.get(nodeToIndex.get(node));
        boolean needComma = false;
        for (VarPredicate vp : npreds) {
            if (needComma) {pred += ", ";}
            pred += vp.toString();
            pred += "}";
            needComma = true;
        }
        pred += "}";

        return pred;
    }

}
