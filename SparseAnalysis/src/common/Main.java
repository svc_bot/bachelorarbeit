package common;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import improved.ImprovedSparseAnalysis;
import simple.SparseAnalysis;
import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.options.Options;
import soot.shimple.ShimpleBody;
import soot.shimple.toolkits.scalar.ShimpleLocalDefs;
import soot.shimple.toolkits.scalar.ShimpleLocalUses;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class Main {
    private static String progToTest = "Prog4Loop";
	
	public static void main(String[] args) {
        final Charset utf8 = StandardCharsets.UTF_8;
        if (!Files.exists(Paths.get("ResultsSimple.txt"), LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.createFile(Paths.get("ResultsSimple.txt"));
                Files.write(Paths.get("ResultsSimple.txt"), ("MethodName, duration in ms, units count, predicates count" + "\n").getBytes(utf8), StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        if (!Files.exists(Paths.get("debug.txt"), LinkOption.NOFOLLOW_LINKS)) {
//            try {
//                Files.createFile(Paths.get("debug.txt"));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        PackManager.v().getPack("stp").add(
				new Transform("stp.myTransform", new BodyTransformer() {
					protected void internalTransform(Body body, String phase, Map options) {
						if(body.getMethod().isEntryMethod()){

                        }
                        Analysis analysis = AnalysisFactory.getAnalysis(AnalysisType.simple, new ExceptionalBlockGraph(body),
                                new ShimpleLocalDefs((ShimpleBody) body),
                                new ShimpleLocalUses((ShimpleBody) body));
                        try {
                            Files.write(Paths.get("ResultsSimple.txt"),
                                    (body.getMethod().getName() + ", " +
                                            analysis.getDuration() + ", " +
                                            analysis.getNodesCount() + ", " +
                                            analysis.getPredicatesCount() + "\n").getBytes(utf8), StandardOpenOption.APPEND);
//                                Files.write(Paths.get("debug.txt"), (((SparseAnalysis)analysis).getIndexToNodeKeySet() + "\n").getBytes(utf8), StandardOpenOption.APPEND);
//                                Files.write(Paths.get("debug.txt"), (((SparseAnalysis)analysis).getNodeToIndexKeySet() + "\n").getBytes(utf8), StandardOpenOption.APPEND);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

					}
				}));
		//G.v().out.println(Arrays.toString(args));
//		args = new String[]{"--d",
//				"/home/alex/workspace/bachelorarbeit/ProjectToTest/sootOutput",
//				"--cp",
//				"/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/home/alex/workspace/bachelorarbeit/ProjectToTest/bin/:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar::/home/alex/workspace/bachelorarbeit/ProjectToTest/src:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/share/java/java-atk-wrapper.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/zipfs.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext/icedtea-sound.jar",
//				"--xml-attributes",
//				"--src-prec",
//				"java",
//				progToTest};
		Options.v().set_interactive_mode(true); // works only in eclipse
		Options.v().full_resolver();
		Options.v().set_via_shimple(true);
		soot.Main.main(args);
	}
}
