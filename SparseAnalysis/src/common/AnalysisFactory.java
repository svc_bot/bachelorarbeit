package common;

import improved.ImprovedSparseAnalysis;
import simple.SparseAnalysis;
import soot.shimple.toolkits.scalar.ShimpleLocalDefs;
import soot.shimple.toolkits.scalar.ShimpleLocalUses;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class AnalysisFactory {

    public static Analysis getAnalysis(AnalysisType atype, ExceptionalBlockGraph graph, ShimpleLocalDefs defs, ShimpleLocalUses uses) {
        if (atype == null) {
            return null;
        } else {
            switch (atype) {
                case simple:
                    return new SparseAnalysis(graph, defs, uses);
                case improved:
                    return new ImprovedSparseAnalysis(graph, defs, uses);
                default:
                    return new SparseAnalysis(graph, defs, uses);
            }
        }
    }
}
