package common;

import soot.Value;
import soot.ValueBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Assertion {
    private ArrayList<String> assertedVars;
    private ArrayList<Value> assertedValues;
    private HashMap<Value, String> substitution;
    private Value condition;
    private int blockIndex;

    public Assertion(Value condition, int blockIndex) {
        this.assertedVars = new ArrayList<>();
        this.assertedValues = new ArrayList<>();
        this.substitution = new HashMap<>();
        this.condition = condition;
        this.blockIndex = blockIndex;
        List<ValueBox> conditionUses = condition.getUseBoxes();
        for (ValueBox use : conditionUses) {
            assertedValues.add(use.getValue());
            String newVar = use.getValue().toString() + "_asserted";
            substitution.put(use.getValue(), newVar);
            assertedVars.add(newVar);
        }
    }

    @Override
    public String toString() {
        return "(" + assertedVars +
                ") = assert(" +
                condition + ")";
    }

    public ArrayList<String> getAssertedVars() {
        return assertedVars;
    }

    public ArrayList<Value> getAssertedValues() {
        return assertedValues;
    }

    public HashMap<Value, String> getSubstitution() {
        return substitution;
    }

    public int getBlockIndex() {
        return blockIndex;
    }

    public Value getCondition() {
        return condition;

    }
}
