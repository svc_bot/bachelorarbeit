package common;

public interface Analysis {
    void doAnalysis();
    long getDuration();
    int getNodesCount();
    int getPredicatesCount();
}
