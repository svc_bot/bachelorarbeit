package common;

public enum InstructionType {
	constAssign, varAssign, complexAssign, assertion, phiFunction, none
}
