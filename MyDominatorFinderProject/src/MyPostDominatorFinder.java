import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import soot.G;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.graph.InverseGraph;


public class MyPostDominatorFinder<N> /*extends MyDominatorFinder<N>*/{
	protected DirectedGraph<N> graph;
	protected BitSet fullSet; // bitset, where every bit is set to true. It is needed to compute intersections
	protected Map<N, BitSet> nodesPostDominators; // Vector, where each bit position corresponds to the index of a node in the graph. 
												  // if 1, then the node is a post dominator of a given node
	protected Map<N, Integer> nodeToIndex = new HashMap<N, Integer>(); // contains indexes of every node
	protected Map<Integer, N> indexToNode = new HashMap<Integer, N>(); // contains reference to every node by its index
	protected List<N> tails; // list of all exit nodes

	public MyPostDominatorFinder(DirectedGraph<N> graph) {
		// easy way: extend from MyDominaorFinder and use inverse graph
//		super(new InverseGraph<N>(graph));
		
		// hard way
		this.graph = graph;
		doAnalysis();
		printPostDominatorsToScreen();
		writeResultsToFile();
	}

	/**
	 * Prints postdominator information to the screen.
	 * 
	 * Iterates through all nodes and prints the derived postdominators
	 * to the screen. Should only be called after doing the analysis,
	 * i.e. , after doAnalysis has been called.
	 */
	private void printPostDominatorsToScreen() {
		for(Iterator<N> i = graph.iterator(); i.hasNext();) {
            N o = i.next();
            G.v().out.println("postdominators for node " + o + " -> " + getPostDominators(o));
		}
	}
	
	private void writeResultsToFile(){
		BufferedWriter output;

		G.v().out.println("Start writing to file");
		
		try {
			File dir = new File("/home/alex/workspace/MyDominatorFinderProject/tests");
			G.v().out.println(dir.getPath());
			File file = new File(dir,"dominators.txt");
			file.createNewFile();
			
			output = new BufferedWriter(new FileWriter(file));
			for(Iterator<N> i = graph.iterator(); i.hasNext();) {
	            N o = i.next();
	            output.write("postdominators for node " + o + " -> " + getPostDominators(o));
	            output.newLine();
			}			
			output.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}
	
	private void doAnalysis() {
		G.v().out.println("Doing analysis..");
		tails = graph.getTails();
		nodesPostDominators = new HashMap<N, BitSet>();
		
        fullSet = new BitSet(graph.size());
        fullSet.flip(0, graph.size()); // set all to true
        
        setUpIndexCorresponence();
        // set up domain of intersection
        for(Iterator<N> i = graph.iterator(); i.hasNext();){
            N o = i.next();
            // tails only postdominate themself
            if(tails.contains(o)){
                BitSet self = new BitSet();
                self.set(nodeToIndex.get(o));
                nodesPostDominators.put(o, self);
            }
            else{
            	// other nodes are set to postdominate everything
                nodesPostDominators.put(o, fullSet);
            }
        }
        
        // see description of the algorithm for more detail
        boolean changed = true;
        while (changed) {
			changed = false;
	        for(Iterator<N> i = graph.iterator(); i.hasNext();){
	            N o = i.next();
	            if(tails.contains(o)) continue; // nothing to do, skip and start with next node
	            
	            BitSet succsIntersect = (BitSet) fullSet.clone();  // contains intersection of all successors of a node o
	            
                //intersect over all successors
                for(Iterator<N> j = graph.getSuccsOf(o).iterator(); j.hasNext();){
                    BitSet predSet = nodesPostDominators.get(j.next());
                    succsIntersect.and(predSet);
                }
                
                BitSet oldSet = nodesPostDominators.get(o);
                //each node post dominates itself
                succsIntersect.set(nodeToIndex.get(o));
                if(!oldSet.equals(succsIntersect)){
                	nodesPostDominators.put(o, succsIntersect);
                	changed = true;
                }
	        }
		}
				
	}
	
	/**
	 * This Method fills nodeToIndex and IndexToNode sets with appropriate values
	 */
	private void setUpIndexCorresponence(){
		int index = 0;
		for(Iterator<N> i = graph.iterator(); i.hasNext();){
			N o = i.next();
			indexToNode.put(index, o);
			nodeToIndex.put(o, index);
			index++;
		}
	}
	
    public DirectedGraph<N> getGraph()
    {
        return graph;
    }   
	
	public List<N> getPostDominators(N node){
        //reconstruct list of post dominators from bitset
        List<N> result = new ArrayList<N>();
        BitSet bitset = nodesPostDominators.get(node);
        for(int i=0;i<bitset.length();i++) {
            if(bitset.get(i)) {            	
                result.add(indexToNode.get(i));
            }
        }
        return result;
	}
	
	public boolean isPostDominatedBy(N node, N postDominator){
		return getPostDominators(node).contains(postDominator);
	}

}
