import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.Unit;
import soot.toolkits.graph.ExceptionalUnitGraph;

import java.util.Map;

public class MyMain {

	public static void main(String[] args) {
		//MyDominatorFinderProject
		 PackManager.v().getPack("jtp").add(
				    new Transform("jtp.myTransform", new BodyTransformer() {
				      protected void internalTransform(Body body, String phase, Map options) {				    	
				        new MyPostDominatorFinder<Unit>(new ExceptionalUnitGraph(body));
				      }
				    }));
		soot.Main.main(args);
	}

}